#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define N 1024 //Максимальная длина строки

int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat, num_fork;
    if (argc < 2) {
        printf("Использование: ./6a file text ...\n");
        exit(-1);
    }


    FILE *fp, *fp2;
    if ((fp = fopen (argv[1], "r")) == NULL) {
        printf ("\e[31mНевозможно открыть файл со строками.\e[0m\n");
        exit (1);
    }
    if ((fp2 = fopen ("temp.txt", "a")) == NULL) {
        printf ("\e[31mНевозможно создать временный файл.\e[0m\n");
        exit (1);
    }    

    int count_string = 0;
    char ch;

    while ((ch = fgetc (fp)) != EOF) {
        if (ch == '\n') {
            count_string++;
            //printf("Найдено строк: %d\n", count_string);
        }
    }
    count_string++;
    printf("Найдено строк: %d\n", count_string);
/*
    int n = 0;
    char **arr = (char**)malloc(sizeof(char*));

    while (!feof(fp)) {
        arr[n] = (char*)malloc(sizeof(char) * N);
        fgets(arr[n], N, fp);
        n++;
    }
*/
    //printf("Введите количество дочерних процессов");
    //scanf ("%d",&num_fork);


    int maxlen = 15;
    rewind(fp);
    num_fork = 4; // пусть дочерних процессов будет 10
    int j = 0;
    char buffer[count_string][N];
    while(!feof(fp)) {
        for (j = 0; j < count_string; j++) {

        memset(buffer[j], '\0',N);
        fgets(buffer[j], N, fp);
        //printf("%s", buffer[j]);
        }

        for (i = 0; i < num_fork; i++) {
            // запускаем дочерние процессы
            pid[i] = fork();
            srand(getpid());
            if (-1 == pid[i]) {
                perror("fork");  //произошла ошибка 
                exit(1); //выход из родительского процесса     
            } else if (0 == pid[i]) {
                
                printf("CHILD: Это %d процесс-потомок СТАРТ!\n", i);
                for (int z = i; z < count_string; z+=num_fork) {
                    //printf("%s", buffer[z]);
                         if ((strlen(buffer[z]) - 2) > maxlen) {
                           fputs(buffer[z], fp2);
                          printf("Обработка %s", buffer[z]); 
                        }
                    //exit(strlen(buffer[z])); // выход из процесс-потомока 
                }

                exit (0);

                
             }
        }            
    //}

        

    //count_string--;
    
    // если выполняется родительский процесс
    //printf("PARENT: Это процесс-родитель!\n");
    // ожидание окончания выполнения всех запущенных процессов
  
    for (i = 1; i < num_fork; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }
   }
    
    fclose (fp);
    return 0;
}



