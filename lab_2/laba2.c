#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */
#define MAX_STRING 10 /* максимальное количество строк */

char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas;  //указатель на массив указателей на строки
	printf ("Введите строки\n");
	mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
    for (int i = 0; i < count ; i++){
        scanf("%s", buffer); // читаем строку в буфер
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer)); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }
    return mas;
}

void printMas(char **mas, int count){

    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем память для массива указателей на строки
}


void sort(v, n)   /* Сортировка в порядке увеличения*/
	char *v[];   
	int n;
	{
	  int gap, i, j;
	  char *temp;
	  for (gap = n/2; gap > 0; gap /= 2)
      	for (i = gap; i < n; i++)
     		for (j = i - gap; j >= 0; j -= gap) {
         		if (strcmp(v[j], v[j+gap]) <= 0) break;
         		temp = v[j];
         		v[j] = v[j+gap];
         		v[j+gap] = temp;
     		}
}


int main(int argc, char **argv){

	char **mas = NULL; //указатель на массив указателей на строки
	printf("Введите количество строк\n");
	int count = 0;
    scanf("%d", &count);

	mtrace();
	mas = readMas(count);
	printf("Введенные строки\n");
	printMas(mas, count);
	sort(mas, count);


	printf("Отсортированный массив\n");
	printMas(mas, count);
	freeMas(mas, count);
}
