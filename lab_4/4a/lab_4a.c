#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 1024 //Максимальная длина строки
int main (int argc, char **argv) {
    if (argc < 3) {
        fprintf (stderr, "\e[33mМало аргументов. Используйте: <\e[0m\e[1m\e[32mимя файла\e[0m\e[33m> <\e[0m\e[1m\e[32mмаксимальное количество символов\e[0m\e[33m>\e[0m\n");
        exit(1);
    }

    if (argc > 3) {
        fprintf (stderr, "\e[33mСлишком много аргументов. Используйте: <\e[0m\e[1m\e[32mимя файла\e[0m\e[33m> <\e[0m\e[1m\e[32mмаксимальное количество символов\e[0m\e[33m>\e[0m\n");
        exit(1);
    }


    FILE *fp, *fp2;
    if ((fp = fopen (argv[1], "r")) == NULL) {
        printf ("\e[31mНевозможно открыть файл со строками.\e[0m\n");
        exit (1);
    }

    
    if ((fp2 = fopen ("temp.txt", "w")) == NULL) {
        printf ("\e[31mНевозможно создать временный файл.\e[0m\n");
        exit (1);
    }
    

    char buffer[N];
    int maxlen = atoi (argv[2]);

    while(!feof(fp)) {
        memset(buffer, '\0',N);
        //printf("%s", buffer);
        if(fgets(buffer, N, fp)) {
            if ((strlen(buffer) - 2) < maxlen) {
                fputs(buffer, fp2);
                printf("%s", buffer); 
            }
        }
    }

    fclose (fp2);
    fclose (fp);

    if (-1 == rename ("temp.txt", argv[1]))
      printf ("\e[31mОшибка перименования\e[0m\n");
    else 
      printf ("\e[32mВыполнено успешно\e[0m\n");

    return 0;
}
