#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dlfcn.h>

int main(int argc, char *argv[]) {
      if (argc < 5) {
        fprintf (stderr, "\e[33mМало аргументов. Используйте: <\e[0m\e[1m\e[32m[-a paramA]\e[0m\e[33m> <\e[0m\e[1m\e[32m[-b paramB]\e[0m\e[33m>\e[0m\n");
        exit(1);
    }

    if (argc > 5) {
        fprintf (stderr, "\e[33mСлишком много аргументов. Используйте: <\e[0m\e[1m\e[32m[-b paramA]\e[0m\e[33m> <\e[0m\e[1m\e[32m[-b paramB]\e[0m\e[33m>\e[0m\n");
        exit(1);
    }

  int opt = 0;
  printf("%s ",argv[0]);
  int a, b = 0;
  while( (opt = getopt(argc, argv, ":a:b:")) != -1) {
	 switch (opt) {
		
    case 'a':
		a = atoi(optarg);
    break;

    case 'b':
		b = atoi(optarg);
    break;
    
    default: /* '?' */
		fprintf(stderr, "Использование a+-b: %s [-a paramA] [-b paramB]\n", argv[0]);
		exit(EXIT_FAILURE);
	 }
	  
    printf("-%c %s ", opt, optarg);
  };

  printf("\n");

  void *ext_library;
  int (*mathfuncplus)(int q, int w);
  int (*mathfuncminus)(int q, int w);

    ext_library = dlopen("./libmath.so",RTLD_LAZY);
    if (!ext_library){
        fprintf(stderr,"dlopen() error: %s\n", dlerror());
        return 1;
    };  

    mathfuncplus = dlsym(ext_library, "plus");
    mathfuncminus = dlsym(ext_library, "minus");


    printf("a+b=%d\n", (*mathfuncplus)(a ,b));
    printf("a-b=%d\n", (*mathfuncminus)(a, b));

    dlclose(ext_library);


  return 0;
}

