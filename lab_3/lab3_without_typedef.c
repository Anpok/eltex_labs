#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
struct sotrudnik{
    char name[50];
    int b_year;
    int n_otdela;
    int oklad;
};

 
void readSotrudnik(struct sotrudnik *st){
	setlocale(LC_ALL, "Rus");
    printf("Введите Фамилию:");
    scanf("%s", st->name);
    printf("Введите год рождения:");
    scanf("%d", &st->b_year);
    printf("Введите номер отдела:");
    scanf("%d", &st->n_otdela);
    printf("Введите оклад:");
    scanf("%d", &st->oklad);	    
}

static int cmp(const void *p1, const void *p2){
    struct sotrudnik * st1 = *(struct sotrudnik**)p1;
    struct sotrudnik * st2 = *(struct sotrudnik**)p2;
    return st2->b_year - st1->b_year;
}
 
int main(int argc, char **argv){
	setlocale(LC_ALL, "Rus");
    int count = 0;
    printf("Введите кол-во сотрудников:");
    scanf("%d", &count);
    struct sotrudnik** st = (struct sotrudnik**)malloc(sizeof(struct sotrudnik*)*count);
    for (int i = 0; i < count ; i++){
        st[i] = (struct sotrudnik*) malloc (sizeof(struct sotrudnik));
        printf("Сотрудник № %d:\n", i+1);        
		readSotrudnik(st[i]);
	} 
    qsort(st, count, sizeof(struct sotrudnik*), cmp);
    printf("Отсортированные сотрудники:\n");
    for (int i = count-1; i >= 0 ; i--){
    	printf("Фамилия сотрудника:%s\n", st[i]->name);
    	printf("Год рождения:%d\n", st[i]->b_year);
    	printf("Номер отдела:%d\n", st[i]->n_otdela);
    	printf("Оклад:%d\n", st[i]->oklad);
    }
    for (int i = 0; i < count; i++)
    {
        free(st[i]);
    }
    free(st);
    return 0;
}
