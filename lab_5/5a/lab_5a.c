#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
int plus (int a, int b);
int minus (int a, int b);

int main(int argc, char *argv[]) {
      if (argc < 5) {
        fprintf (stderr, "\e[33mМало аргументов. Используйте: <\e[0m\e[1m\e[32m[-a paramA]\e[0m\e[33m> <\e[0m\e[1m\e[32m[-b paramB]\e[0m\e[33m>\e[0m\n");
        exit(1);
    }

    if (argc > 5) {
        fprintf (stderr, "\e[33mСлишком много аргументов. Используйте: <\e[0m\e[1m\e[32m[-b paramA]\e[0m\e[33m> <\e[0m\e[1m\e[32m[-b paramB]\e[0m\e[33m>\e[0m\n");
        exit(1);
    }

  int opt = 0;
  printf("%s ",argv[0]);
  int a, b, y, z = 0;
  while( (opt = getopt(argc, argv, ":a:b:")) != -1) {
	 switch (opt) {
		
    case 'a':
		a = atoi(optarg);
    break;

    case 'b':
		b = atoi(optarg);
    break;

    
    default: /* '?' */
		fprintf(stderr, "Использование a+-b: %s [-a paramA] [-b paramB]\n", argv[0]);
		exit(EXIT_FAILURE);
	 }
	  
    printf("-%c %s ", opt, optarg);
  };

  y = plus(a, b);
  z = minus (a, b);

  printf("\na+b=%d\na-b=%d\n", y, z);
 

  return 0;
}

